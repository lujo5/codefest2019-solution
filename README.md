# Codefest 2019 - solution

The final solution for the Codefest 2019 hackathon. Machine learning algorithms for router classification, LED and cable detection. 
Also, this project contains Facebook ChatBot implementation which uses trained neural networks to detect problems.

## Project structure

### router-classificator

Python program for router type classification using machine learning with squeeznet and vgg18 variation models.


### router-predicter

Python flask application with exposed HTTP endpoints for performing evaluation of images for router type, LEDs and cables status.


### solution-validation

Python command line tool used for bulk validation of trained models and algorithms.


### support-web

Web application for viewing the status of ChatBot performance and created tasks through facebook messenger.


### tech-support-solution

Backend application used as ChatBot service which calls other service when users uploads images of routers through messenger.