import sys

import numpy as np
from PIL import Image
from keras.models import load_model

IMG_DIR = '/opt/lampp/htdocs/tech-support-solution/storage/app/images/'


def main():
    img_size = 699
    model = load_model('../model/model.10-2.03.h5')

    model._make_predict_function()

    # Loading and preparing image
    image_data = []
    img = Image.open(IMG_DIR + str(sys.argv[1]))
    img = img.convert('L')
    img = img.resize((img_size, img_size), Image.ANTIALIAS)
    img = np.array(img)
    image_data.append([img, 'test'])

    images = np.array([i[0] for i in image_data]).reshape(-1, img_size, img_size, 1)
    preds = model.predict(np.expand_dims(images[0], axis=0))
    values = preds.tolist()
    if values[0][0] == 1:
        print('1')  # Black
        predicted = 1
    else:
        print('0')  # White
        predicted = 0
    with open("../out/results.txt", "a") as file:
        file.write('Img: ' + str(sys.argv[1]) + ' - ' + str(predicted))
    return int(predicted)


if __name__ == '__main__':
    main()
