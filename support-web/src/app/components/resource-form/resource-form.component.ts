import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup} from '@angular/forms';
import {ValidatorFn} from '@angular/forms/src/directives/validators';

@Component({
  selector: 'app-resource-form',
  templateUrl: './resource-form.component.html',
  styleUrls: ['./resource-form.component.css']
})
export class ResourceFormComponent implements OnInit, OnChanges {
  @Input() title: string = 'Resource';
  @Input() subtitle: string = 'Edit form';
  @Input() resource: any;
  @Input() fields: Array<FormField> = [];
  @Input() buttons: Array<FormButton> = [];

  @Output() button = new EventEmitter<FormEvent>();

  loading: boolean = true;
  fieldType = FieldType;

  resourceForm: FormGroup;

  constructor(private _fb: FormBuilder) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.resource) {
      this.initForm();
      this.loading = false;
    }
  }

  ngOnInit(): void {
  }

  emitButton(name): void {
    if (this.resourceForm.invalid) {
      this.markFormGroupTouched(this.resourceForm);
    }
    let values = this.resourceForm.getRawValue();
    this.button.emit({valid: !this.resourceForm.invalid, name: name, data: this.prepareData(values)});
  }

  getError(field: FormField): any[] {
    let control = this.getControl(field);
    control.markAsTouched();
    if (this.isRequired(control)) {
      return ['components.resourceForm.required', {}];
    }
    if (this.isEmail(control)) {
      return ['components.resourceForm.invalidEmail', {}];
    }
    if (this.isMaxLength(control)) {
      return ['components.resourceForm.maxlength', {value: control.getError('maxlength').requiredLength}];
    }
    if (this.isMinLength(control)) {
      return ['components.resourceForm.minlength', {value: control.getError('minlength').requiredLength}];
    }
    if (this.isMaxValue(control)) {
      return ['components.resourceForm.max', {value: control.getError('max').max}];
    }
    if (this.isMinValue(control)) {
      return ['components.resourceForm.min', {value: control.getError('min').min}];
    }
    return ['components.resourceForm.invalid', {}];
  }

  isRequired(control: any): boolean {
    return control && control.hasError('required') && (control.dirty || control.touched);
  }

  isEmail(control: any): boolean {
    return control && control.hasError('email') && (control.dirty || control.touched);
  }

  isMaxLength(control: any): boolean {
    return control && control.hasError('maxlength') && (control.dirty || control.touched);
  }

  isMinLength(control: any): boolean {
    return control && control.hasError('minlength') && (control.dirty || control.touched);
  }

  isMaxValue(control: any): boolean {
    return control && control.hasError('max') && (control.dirty || control.touched);
  }

  isMinValue(control: any): boolean {
    return control && control.hasError('min') && (control.dirty || control.touched);
  }

  isInvalid(field: FormField): boolean {
    let control = this.getControl(field);
    return control && control.invalid && (control.dirty || control.touched);
  }

  compareByName(f: any, s: any): boolean {
    return f === s;
  }

  compareById(f: any, s: any): boolean {
    return f && s && f.id === s.id;
  }

  private getControl(field: FormField): AbstractControl {
    if (!this.resourceForm) {
      return null;
    }
    return this.resourceForm.get(field.name);
  }

  private initForm(): void {
    let group = {};
    this.fields.forEach(field => {
      if (field.type === FieldType.CHECKBOX) {
        group[field.name] = this._fb.array(this.getItems(field), field.validators || []);
      } else if (field.type === FieldType.RADIOBUTTON) {
        group[field.name] = [this.getOptionsValue(field) || '', field.validators || []];
      } else {
        group[field.name] = [this.resource[field.name] || '', field.validators || []];
      }
    });
    this.resourceForm = this._fb.group(group);
  }

  private markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach(control => {
      control.markAsTouched();
      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }

  private getOptionsValue(field): any {
    let valueKey = field.options['valueKey'];
    for (let i = 0; i < field.options['values'].length; i++) {
      let value = field.options['values'][i]['value'][valueKey];
      if (this.resource[field.name][valueKey] === value) {
        return field.options['values'][i]['value'];
      }
    }
  }

  private getItems(field): any[] {
    let valueKey = field.options['valueKey'];
    let controls = field.options['values'].map(() => this._fb.control(false));
    for (let i = 0; i < field.options['values'].length; i++) {
      let value = field.options['values'][i]['value'][valueKey];
      for (let j = 0; j < this.resource[field.name].length; j++) {
        let resVal = this.resource[field.name][j][valueKey];
        if (resVal === value) {
          controls[i].setValue(true);
        }
      }
    }
    return controls;
  }

  private prepareData(values: any): any {
    let data = {};
    Object.keys(values).forEach(key => {
      let value = values[key];
      let field = this.fields.find(f => f.name === key && f.type === FieldType.CHECKBOX);
      if (field && value.length > 0) {
        let realObjects = [];
        for (let i = 0; i < value.length; i++) {
          let option = field.options['values'][i];
          if (value[i] === true) {
            realObjects.push(option['value']);
          }
        }
        data[key] = realObjects;
      } else {
        data[key] = value;
      }
    });
    return data;
  }
}

export interface FormField {
  name: string;
  label: string;
  type: FieldType;
  hint?: string;
  options?: InputOptions | TextareaOptions | SelectOptions | ToggleOptions | DatepickerOptions | RadiobuttonOptions | CheckboxOptions;
  validators?: Array<ValidatorFn>;
  classes?: string;
}

export interface FormButton {
  name: string;
  text?: string;
  icon?: string;
  color?: string;
  classes?: string;
}

export interface FormEvent {
  valid: boolean;
  name: string;
  data?: any;
}

export enum FieldType {
  INPUT,
  TEXTAREA,
  SELECT,
  TOGGLE,
  DATEPICKER,
  RADIOBUTTON,
  CHECKBOX,
  TIMEPICKER
}

export interface InputOptions {
  type?: 'text' | 'number' | 'password' | 'email' | 'tel' | 'url';
  color?: string;
}

export interface TextareaOptions {
  maxRows?: number;
  minRows?: number;
  color?: string;
}

export interface SelectOptions {
  values?: Array<{ value: any, name: string }>;
  color?: string;
}

export interface ToggleOptions {
  labelPosition?: 'before' | 'after';
  color?: string;
}

export interface DatepickerOptions {
  startDate?: Date;
  startView?: 'month' | 'year' | 'multi-year';
  maxDate?: Date;
  minDate?: Date;
  color?: string;
}

export interface RadiobuttonOptions {
  labelPosition?: 'before' | 'after';
  orientation?: 'horizontal' | 'vertical';
  values?: Array<{ value: any, name: string }>;
  color?: string;
}

export interface CheckboxOptions {
  labelPosition?: 'before' | 'after';
  orientation?: 'horizontal' | 'vertical';
  valueKey?: any;
  values?: Array<{ value: any, name: string }>;
  color?: string;
}
