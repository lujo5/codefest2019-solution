import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {GenericResponse, RestService} from 'ngx-restful';
import {Task} from '../models/task.model';
import {environment} from '../../environments/environment';

@Injectable()
export class TaskService extends RestService<Task, GenericResponse> {

  constructor(protected http: HttpClient) {
    super(http);
  }

  getBaseUrlPath(): string {
    return environment.baseUrl + 'tasks';
  }
}
