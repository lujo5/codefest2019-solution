# ChatBot Support Web
Frontend SPA application for viewing tasks created and resolved by ChatBot backend solution.

## Installation
1. Run: npm install
2. In src/environments/environment.ts update "apiBase" with backend url of deployed ChatBot solution
3. Run: npm run start
4. Navigate to http://localhost:4200 to check if app is working
