<?php

return [
    'prediction' => [
        'url' => env('ROUTER_PREDICTION_SERVICE', 'http://127.0.0.1:5000'),
    ],
    'botman' => [
        'facebook' => [
            'token' => env('FACEBOOK_TOKEN', ''),
            'verification' => env('FACEBOOK_VERIFICATION', ''),
            'app_secret' => env('FACEBOOK_APP_SECRET', ''),
        ]
    ],
    'cors' => [
        'origin' => env('ACCESS_CONTROL_ALLOW_ORIGIN', '*'),
        'methods' => env('ACCESS_CONTROL_ALLOW_METHODS', 'POST, GET, OPTIONS, PUT, DELETE'),
        'credentials' => env('ACCESS_CONTROL_ALLOW_CREDENTIALS', 'true'),
        'maxage' => env('ACCESS_CONTROL_MAX_AGE', '86400'),
        'headers' => env('ACCESS_CONTROL_ALLOW_HEADERS', 'Content-Type, Authorization, X-Requested-With, App-language, App-standalone, X-Result-Count, X-Total-Count'),
        'expose' => env('ACCESS_CONTROL_EXPOSE_HEADERS', 'X-Result-Count, X-Total-Count')
    ]
];