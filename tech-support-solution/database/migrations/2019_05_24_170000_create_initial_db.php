<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInitialDb extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('user', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('username')->unique();
            $table->string('phone_number')->unique()->nullable();
            $table->string('password_hash');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('account_type')->default('USER');
            $table->string('image_file')->nullable();
            $table->boolean('enabled')->default(true);
            $table->timestamps();
            $table->index(['id', 'email', 'phone_number', 'username']);
        });

        Schema::create('message', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type')->default('INBOUND'); // INBOUND, OUTBOUND
            $table->string('msg_id')->nullable();
            $table->string('sender')->nullable();
            $table->string('recipient')->nullable();
            $table->string('text')->nullable();
            $table->string('image_url', 512)->nullable();
            $table->string('image_file')->nullable();
            $table->timestamps();
            $table->index(['id']);
        });

        Schema::create('task', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status')->default('TODO'); // T*ODO, IN_PROGRESS, DONE
            $table->string('sender')->nullable();
            $table->string('name')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
            $table->index(['id']);
        });

        Schema::create('equipment', function (Blueprint $table) {
            $table->increments('id');
            $table->string('serial_number')->unique();
            $table->string('name')->nullable();
            $table->string('manufacturer')->nullable();
            $table->string('description')->nullable();
            $table->string('image_file')->nullable();
            $table->timestamps();
            $table->index(['id', 'serial_number']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('user');
        Schema::dropIfExists('message');
        Schema::dropIfExists('task');
        Schema::dropIfExists('equipment');
    }
}
