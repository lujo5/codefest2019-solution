<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Laravel\Lumen\Auth\Authorizable;
use Laravel\Passport\HasApiTokens;

class User extends Model implements AuthenticatableContract, AuthorizableContract {
    use HasApiTokens, Authenticatable, Authorizable, Notifiable;

    protected $table = 'user';

    protected $fillable = [
        'email',
        'username',
        'password_hash',
        'first_name',
        'last_name',
        'account_type',
        'image_file',
        'enabled',
    ];

    protected $hidden = [
        'password_hash'
    ];

    public function delete() {
        $this->revokeAllTokens();
        return parent::delete();
    }

    public function save(array $options = []) {
        if ($this->enabled === false) {
            $this->revokeAllTokens();
        }
        return parent::save();
    }

    /**
     * Find user by username or email field which is used in the OAuth2 authentication.
     *
     * @param string $username
     */
    public function findForPassport($username) {
        $credential = trim($username);
        $user = $this->where('enabled', true)->where(function ($q) use ($credential) {
            $q->orWhere('username', 'ilike', strtolower($credential))->orWhere('email', 'ilike', strtolower($credential));
        })->first();
        if ($user === null) {
            Log::error('Login attempt with invalid credential: "' . $credential . '"');
        }
        return $user;
    }

    /**
     * Password validation callback.
     *
     * @param type $password
     * @return boolean Whether the password is valid
     */
    public function validateForPassportPasswordGrant($password) {
        $result = Hash::check(trim($password), $this->password_hash);
        if (!$result) {
            Log::error('Login attempt failed with invalid password');
        } else {
            $req = app('request');
            $user = User::where('username', $req->input('username'))->orWhere('email', $req->input('username'))->first();
            $req->setUserResolver(function () use ($user) {
                return $user;
            });
        }
        return $result;
    }

    public function routeNotificationForMail($notification) {
        return $this->email;
    }

    private function revokeAllTokens() {
        foreach ($this->tokens as $token) {
            $token->revoke();
        }
    }
}
