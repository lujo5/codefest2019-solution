<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model {

    public const INBOUND_TYPE = 'INBOUND';
    public const OUTBOUND_TYPE = 'OUTBOUND';

    protected $table = 'message';

    protected $fillable = [
        'type',
        'msg_id',
        'sender',
        'recipient',
        'text',
        'image_url',
        'image_file',
    ];
}