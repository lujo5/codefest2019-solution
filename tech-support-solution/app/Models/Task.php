<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model {

    public const STATUSES = ['TODO', 'IN_PROGRESS', 'DONE'];

    protected $table = 'task';

    protected $fillable = [
        'status',
        'sender',
        'name',
        'description'
    ];
}