<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Equipment extends Model {

    protected $table = 'equipment';

    protected $fillable = [
        'serial_number',
        'name',
        'manufacturer',
        'description',
        'image_file'
    ];
}