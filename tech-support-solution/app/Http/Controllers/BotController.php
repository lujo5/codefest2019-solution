<?php

namespace App\Http\Controllers;

use App\Service\BotService;
use BotMan\BotMan\BotManFactory;
use BotMan\BotMan\Drivers\DriverManager;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\Drivers\Facebook\Extensions\ButtonTemplate;
use BotMan\Drivers\Facebook\Extensions\ElementButton;
use BotMan\Drivers\Facebook\FacebookDriver;
use BotMan\Drivers\Facebook\FacebookImageDriver;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Services\ImageService;
use Illuminate\Support\Facades\Log;
use Laravel\Lumen\Routing\Controller as BaseController;

class BotController extends BaseController {

    function listen(Request $request) {
        DriverManager::loadDriver(FacebookDriver::class);
        DriverManager::loadDriver(FacebookImageDriver::class);
        $botman = BotManFactory::create(config('util.botman'));

        $botman->receivesImages(function ($bot, $images) {
            $botService = new BotService($bot->getUser());
            $state = $botService->getCurrentState();
            $action = 'success';
            $imageNames = [];
            foreach ($images as $image) {
                $url = $image->getUrl(); // The direct url
                Log::info('img url: ' . $url);
                $imageName = ImageService::fetchImage($url);  // Saves image to filesystem and returns image name
                array_push($imageNames, $imageName);
            }
            Log::info('Images count: ' . count($imageNames));
            if (count($imageNames) < 1) {
                Log::info('SKipping image read');
                return;
            }
            if ($state === '2') {
                foreach ($imageNames as $img) {
                    $result = $this->executeClassification($img);
                    Log::info('Classification result: ' . $result);
                    if ($result == '1') { // Black
                        $bot->reply('Vaš ruter je Ubee TWV625');
                        $bot->userStorage()->save([
                            'color' => 1
                        ]);
                    } else if ($result == '0') { // White
                        $bot->reply('Vaš ruter je Huawei HG8245');
                        $bot->userStorage()->save([
                            'color' => 0
                        ]);
                    }
                    break;
                }
            } else if ($state === '5' || $state === '6') {
                $error_type = '0';
                foreach ($imageNames as $img) {
                    $store = $bot->userStorage()->all();
                    $color = $store[0]['color'];
                    Log::info(print_r($color, true));
                    if ($state === '5') {
                        $error_type = $this->executeLedDetector($img, $color);
                    }
                    if ($state === '6') {
                        $error_type = $this->executeCabelDetector($img, $color);
                    }
                }
                if ($state === '5') {
                    switch ($error_type) {
                        case '1':
                            $bot->reply('Sve je u redu s LEDicama.');
                            break;
                        case '0':
                            $bot->reply('LEDice nisu upaljene, provjerite je li ruter uključen i napajanje spojeno.');
                            break;
                    }
                }
                if ($state === '6') {
                    switch ($error_type) {
                        case '1':
                            $bot->reply('Sve je u redu s kablovima.');
                            break;
                        case '0':
                            $bot->reply('Kablovi nisu dobro spojeni, spojite kablove prema uputama.');
                            break;
                    }
                }
            }

            $botService->goToNextState($action);
        });

        $botman->hears('(.*)', function ($bot, $text) {
            $botService = new BotService($bot->getUser());
            Log::info('Bot heard text: ' . $text);
            $action = '';
            switch ($botService->getCurrentState()) {
                case "0":
                    $bot->reply('Pozdrav!');
                    $bot->reply(Question::create('Znate li model vašeg rutera?')->addButtons([
                        Button::create('Znam')->value('znam'),
                        Button::create('Ne znam')->value('ne'),
                    ]));
                    break;
                case "1":
                    if (strpos($text, 'znam') === 0) {
                        $bot->reply('Super, molimo pošaljite slike vašeg rutera s prednje strane');
                        $action = 'knows';
                    } else {
                        $bot->reply('Pošaljite nam sliku Vašeg rutera');
                        $action = 'dont_know';
                    }
                    break;
                case "5":
                    $bot->reply('Pošaljite slike vašeg rutera s prednje strane');
                    $action = 'stay';
                    break;
                case "6":
                    $bot->reply('Pošaljite slike vašeg rutera sa stražnje strane');
                    $action = 'stay';
                    break;
                case "7":
                    $bot->reply(Question::create('Jeste li uspjeli popraviti problem?')->addButtons([
                        Button::create('Jesam')->value('jesam'),
                        Button::create('Nisam')->value('ne'),
                    ]));
                    $action = 'tell_problem';
                    break;
                case "8":
                    if (strpos($text, 'jesam') === 0 || $text === '%%%_IMAGE_%%%') {
                        $bot->reply('Super!');
                        $action = 'fixed';
                    } else {
                        $bot->reply('Pošaljite nam još fotografija');
                        $action = 'not_fixed';
                    }
            }
            $botService->goToNextState($action);
        });

        $botman->listen();
    }

    /**
     * Sends image name to detect the router type.
     *
     * @param $imageName string Image name
     * @return int|\Psr\Http\Message\StreamInterface
     */
    private function executeClassification($imageName) {
        $client = new Client(['base_uri' => config('util.prediction.url')]);
        try {
            $response = $client->request('GET', 'predict?name=' . $imageName);
            return $response->getBody();
        } catch (\Exception $e) {
            Log::info('Failed Flask /predict service request');
            return -1;
        }
    }

    /**
     * Sends image name to detect status of LEDs.
     *
     * @param $imageName string Image name
     * @param $color string Detected router color (type)
     * @return int|\Psr\Http\Message\StreamInterface
     */
    private function executeLedDetector($imageName, $color) {
        $client = new Client(['base_uri' => config('util.prediction.url')]);
        try {
            $response = $client->request('GET', 'led?name=' . $imageName . '&color=' . $color);
            return $response->getBody();
        } catch (\Exception $e) {
            Log::info('Failed Flask /led service request');
            return -1;
        }
    }

    /**
     * Sends image name to detect the status of cabels.
     *
     * @param $imageName string Image name
     * @param $color string Detected router color (type)
     * @return int|\Psr\Http\Message\StreamInterface
     */
    private function executeCabelDetector($imageName, $color) {
        $client = new Client(['base_uri' => config('util.prediction.url')]);
        try {
            $response = $client->request('GET', 'cabel?name=' . $imageName . '&color=' . $color);
            return $response->getBody();
        } catch (\Exception $e) {
            Log::info('Failed Flask /cabel service request');
            return -1;
        }
    }
}
