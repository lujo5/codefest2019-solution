<?php


namespace App\Http\Controllers;


use App\Services\ImageService;
use Laravel\Lumen\Routing\Controller as BaseController;

class AssetController extends BaseController {

    public function image($filename) {
        if (empty($filename) || $filename === 'undefined' || $filename == 'null') {
            return response('', 400);
        }
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        return response(ImageService::rawImage($filename))
            ->header('Content-Type', 'image/' . ($ext ?: 'image'))
            ->header('Pragma', 'public')
            ->header('Content-Disposition', 'inline; filename="' . $filename . '"')
            ->header('Cache-Control', 'max-age=60, must-revalidate');
    }
}