<?php

namespace App\Http\Controllers\Rest;


use App\Models\Task;
use Illuminate\Database\Eloquent\Model;
use Lujo\Lumen\Rest\RestController;

class TaskController extends RestController {

    /**
     * Return the specific model object of a resource for child controller.
     *
     * @return Model
     */
    protected function getModel() {
        return new Task();
    }
}