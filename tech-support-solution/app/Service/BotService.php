<?php


namespace App\Service;


use App\Models\Task;
use BotMan\BotMan\Users\User;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class BotService {

    private $user;

    public function __construct(User $user) {
        $this->user = $user;
        Log::info('user_id: ' . $user->getId());
    }

    public function getCurrentState() {
        $state = Cache::get($this->user->getId());
        if ($state === null) {
            Log::info('No state, setting to 0');
            $state = '0';
        }
        return $state;
    }

    public function getNextState($action) {
        return StateService::getNextState($this->getCurrentState(), $action);
    }

    public function goToNextState($action) {
        $next_state = $this->getNextState($action);
        $this->updateTask($next_state, $action);
        Cache::put($this->user->getId(), $next_state, 300);
    }

    /**
     * Persists the support task object in the database on any state transition.
     *
     * @param $next_state string Name of the next state the bot is transitioning to.
     * @param $action string Action name.
     */
    private function updateTask($next_state, $action) {
        if ($next_state === '1') {
            $task = new Task();
            $task->status = 'IN_PROGRESS';
            $task->name = $this->user->getFirstName() . ' ' . $this->user->getLastName();
            $task->description = 'Pomoć za router Ubee TWV625. Messenger: https://www.facebook.com/messages/t/' .
                $this->user->getId();
            $task->save();
        }
        if ($next_state === '0' && $action === 'fixed') {
            $task = Task::where('status', 'IN_PROGRESS')->first();
            if ($task) {
                $task->status = 'DONE';
                $task->save();
            }
        }
        if ($next_state === '5' && $action === 'fail') {
            $task = Task::where('status', 'IN_PROGRESS')->first();
            if ($task) {
                $task->status = 'TODO';
                $task->save();
            }
        }
    }
}