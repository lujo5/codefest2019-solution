# Router classificator
Training model using Keras and TensorFlow frameworks with vgg16 and squeezenet models variations. 
In order to train and test the model, add images to resources/train and resources/test models in sub-folders /black and /white depending
of the router type.

Start the training model by executing main.py script.

The training model output is generated in /out folder.