import csv
import numpy as np
from PIL import Image
from keras.models import load_model
import os
import cv2
from detect_led_status import image_has_led

IMG_SIZE = 699
DIR_PATH = "validation/validation_set/upaljeno_ugaseno/"
# DIR_PATH = "training_set/black/lampice/ugaseno/"

image_data = []
model = load_model('model-borw-cnn.h5')


def predict(image_file):
    image_data = []

    img = Image.open(DIR_PATH + image_file)
    img = img.convert('L')
    img = img.resize((IMG_SIZE, IMG_SIZE), Image.ANTIALIAS)
    img = np.array(img)
    image_data.append([img, 'test'])
    #
    images = np.array([i[0] for i in image_data]).reshape(-1, IMG_SIZE, IMG_SIZE, 1)
    #
    preds = model.predict(np.expand_dims(images[0], axis=0))
    values = preds.tolist()
    if values[0][0] > values[0][1]:
        # black
        return image_has_led(cv2.imread(DIR_PATH + image_file), 10, (30, 100, 100), (50, 255, 255))
    else:
        # white
        return image_has_led(cv2.imread(DIR_PATH + image_file), 9, (23, 75, 88), (100, 255, 255))


with open('validation/validation_results/green_light_technologies_upaljeno_ugaseno.csv', mode='w', newline='') as csv_file:
    csv_writer = csv.writer(csv_file)
    csv_writer.writerow(['name', 'y_predicted'])

    files = os.listdir(DIR_PATH)

    for image in files:
        csv_writer.writerow([image, "{:.1f}".format(predict(image))])
