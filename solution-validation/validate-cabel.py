import csv
import os
import cv2
import numpy as np
from PIL import Image
from keras.models import load_model
from detect_cable_status import image_has_cable

IMG_SIZE = 699
DIR_PATH = "validation/validation_set/istekano_ustekano/"
# DIR_PATH = "training_set/black/kablovi/istekano/"

image_data = []
model = load_model('model-borw-cnn.h5')


def predict(image_file):
    image_data = []

    img = Image.open(DIR_PATH + image_file)
    img = img.convert('L')
    img = img.resize((IMG_SIZE, IMG_SIZE), Image.ANTIALIAS)
    img = np.array(img)
    image_data.append([img, 'test'])

    images = np.array([i[0] for i in image_data]).reshape(-1, IMG_SIZE, IMG_SIZE, 1)

    preds = model.predict(np.expand_dims(images[0], axis=0))
    values = preds.tolist()
    if values[0][0] > values[0][1]:
        # black
        return image_has_cable(cv2.imread(DIR_PATH + image_file), 5, 46, 205, 345, 5.75)

    else:
        # white
        return image_has_cable(cv2.imread(DIR_PATH + image_file), 50, 103, 334, 374, 0)


with open('validation/validation_results/green_light_technologies_istekano_ustekano.csv', mode='w', newline='') as csv_file:
    csv_writer = csv.writer(csv_file)
    csv_writer.writerow(['name', 'y_predicted'])

    files = os.listdir(DIR_PATH)

    for image in files:
        csv_writer.writerow([image, "{:.1f}".format(predict(image))])
